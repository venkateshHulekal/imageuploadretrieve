sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function (Controller) {
	"use strict";

	return Controller.extend("com.venkatesh.ImageUploadDownload.controller.View1", {
		onInit: function () {
			this._oViewModel = new sap.ui.model.json.JSONModel({
				imageData: ""
			});
			this.getView().setModel(this._oViewModel, "viewModel");
		},
		fileUploaderChange: function (oControlEvent) {
			var reader = new FileReader();
			var oFiles = oControlEvent.getParameters().files;
			for (var i = 0; i < oFiles.length; i++) {
				reader.readAsDataURL(oFiles[i]);
			}
			reader.onloadend = function () {
				this._oViewModel.setProperty("/imageData", reader.result);
				//this.displayImage();
				this.uploadImage();
			}.bind(this);
		},

		displayImage: function () {
			this.getView().byId("img").setSrc(this._oViewModel.getProperty("/imageData"));
		},

		uploadImage: function () {
			var mainModel = this.getOwnerComponent().getModel();

			var oDataObject = {};
			oDataObject = {
				"id": "0",
				"image": this._oViewModel.getProperty("/imageData")
			};
			
			mainModel.create("/IMAGES", oDataObject, {
				success: function (oData, oResponse) {
					this.getView().getModel().resetChanges();                         
				}.bind(this),
				error: function (error) {
				}
			});
		}

	});
});